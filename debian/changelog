post-el (1:2.8-2) unstable; urgency=medium

  * Fix missed Hompage field
  * Bump debhelper from old 11 to 13
  * Standards-Version to 4.7.0
  * Add debian/upstream/metadata
  * Add emacsen-compat file (Closes: 868560)

 -- Eric Dorland <eric@debian.org>  Sat, 25 Jan 2025 00:12:25 -0500

post-el (1:2.8-1) unstable; urgency=medium

  * New upstream release (Closes: #996755, #1052195)
  * Switch upstream to https://github.com/Boruch-Baum/post-mode

 -- Eric Dorland <eric@debian.org>  Fri, 24 Jan 2025 23:00:11 -0500

post-el (1:2.6-2) unstable; urgency=medium

  * Update Vcs-* fields to salsa.debian.org
  * Standards-Version to 4.2.1
  * Switch to debhelper v11
  * Add Rules-Requires-Root: no
  * Update to a machine readable copyright file

 -- Eric Dorland <eric@debian.org>  Wed, 14 Nov 2018 00:46:01 -0500

post-el (1:2.6-1) unstable; urgency=medium

  * New upstream release. (Closes: #477135)
  * debian/patches: Remove now upstream patches.
  * debian/control: Update Homepage.
  * debian/control: Standards-Version to 3.9.6.1.
  * debian/watch: Point at new github home.
  * debian/gbp.conf: Use pristine-tar and upstream-vcs-tag.

 -- Eric Dorland <eric@debian.org>  Sun, 31 May 2015 20:03:13 -0400

post-el (1:2.5-4) unstable; urgency=medium

  * debian/control: Make VCS fields canonical.
  * debian/control: Upgrade to Standards-Version to 3.9.5.

 -- Eric Dorland <eric@debian.org>  Sun, 14 Sep 2014 19:09:41 -0400

post-el (1:2.5-3) unstable; urgency=low

  * debian/watch: Add watch file.
  * post.el, debian/patches/02-attachment-space-escaping.diff: Move
    attachment space escaping to a patch.
  * post.el, debian/patches/01-remove-insert-string.diff: Move
    insert-string diff to a patch.
  * post.el, debian/patches/03-fix-custom-types.diff: Move type fixing to a
    patch.
  * post.el, debian/patches/04-regex-fixups.diff: Move regexp fixups diff
    into a patch.
  * post.el, debian/patches/05-spelling-fixes.diff: Move spelling fixes
    into a patch.
  * debian/source/format: Convert to 3.0 quilt format.
  * debian/control, debian/compat: Upgrade to debhelper v9.
  * debian/control: Standards-Version to 3.9.4.
  * debian/control: Depend on emacs instead of emacs23. Thanks Matt
    Kraai. (Closes: #754039)

 -- Eric Dorland <eric@debian.org>  Sun, 14 Sep 2014 00:19:10 -0400

post-el (1:2.5-2) unstable; urgency=low

  * post.el, debian/post-el.emacsen-startup: Fix for new mutt filename
    format. (Closes: #568943)
  * debian/control: Standards-Version to 3.8.4, depend on emacs23 instead
    of 22.
  * post-el: Patch from Philip J. Hollenback to add escaping to spaces in
    attachment filenames. (Closes: #421604)

 -- Eric Dorland <eric@debian.org>  Sun, 28 Feb 2010 19:41:03 -0500

post-el (1:2.5-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Upgrade Standards-Version to 3.8.1.
    - Add Homepage and Vcs-* headers.
    - Depend on emacs22 instead of emacs21.
    - Set build depends on debhelper to >= 7.0.
    - Add ${misc:Depends}.
  * debian/copyright: Add copyright to the copyright file.
  * debian/compat: Bump to version 7.
  * debian/rules: Convert to a simple dh rules file.

 -- Eric Dorland <eric@debian.org>  Sun, 26 Apr 2009 02:42:01 -0400

post-el (2004.07.23-5) unstable; urgency=low

  * post.el: Add some missing characters to the post-url-pattern. 

 -- Eric Dorland <eric@debian.org>  Thu,  8 Mar 2007 03:50:37 -0500

post-el (2004.07.23-4) unstable; urgency=low

  * post.el: 
    - Replace how-many with a wrapper for emacs >= 22. (Closes: #364525)
    - Replace insert-string calls with insert for emacs >= 22.

 -- Eric Dorland <eric@debian.org>  Tue,  6 Mar 2007 03:59:54 -0500

post-el (2004.07.23-3) unstable; urgency=low

  * debian/control: 
    - Upgrade Standards-Version to 3.7.2.0.
    - Use Build-Depends instead of Build-Depends-Indep for things 
      that need to be there for the clean target.

 -- Eric Dorland <eric@debian.org>  Thu, 15 Jun 2006 22:37:48 -0400

post-el (2004.07.23-2) unstable; urgency=low

  * post.el: 
    - Adapted a patch from Per Olofsson to have it launch with
      muttng as well as mutt. (Closes: #319203)
    - Remove the \s in front of emoticons. 

 -- Eric Dorland <eric@debian.org>  Fri, 29 Jul 2005 16:54:17 -0400

post-el (2004.07.23-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: 
    - Update website and upstream email address.
    - Add author's copyright notice. 
  * debian/rules: Update Standards-Version to 3.6.1.1.
  * post.el: 
    - Add : to post-url-pattern to handle port specs in URLs.
    - Add url, email and quote patterns to ispell skip list. 
    - Make the smart attachment detection not check quoted message 
      text, which really annoys me. 

 -- Eric Dorland <eric@debian.org>  Mon,  7 Feb 2005 01:35:18 -0500

post-el (2002.04.22-8) unstable; urgency=low

  * debian/control: Standards-Version to 3.5.8.0.
  * debian/copyright: Fixes so lintian will stop complaining.
  * post.el: 
    + Make flyspell ignore some headers from spell checking.
    + Make some string types into the more correct regexp type.

 -- Eric Dorland <eric@debian.org>  Sun, 16 Feb 2003 02:14:58 -0500

post-el (2002.04.22-7) unstable; urgency=low

  * debian/post-el.emacsen-install: chmod compiled files for weird
    umasks. (Closes: #152542)

 -- Eric Dorland <eric@debian.org>  Thu, 11 Jul 2002 16:38:21 -0400

post-el (2002.04.22-6) unstable; urgency=low

  * Fix post-mail-message regexp in
    debian/post-el.emacsen-startup. (Closes: #148058)

 -- Eric Dorland <eric@debian.org>  Fri, 24 May 2002 13:51:29 -0400

post-el (2002.04.22-5) unstable; urgency=low

  * Updated to debhelper 4.
    + Added debian/compat file.
    + Updated build-deps.
    + Removed export DH_COMPAT.
    + Started using dh_install. Added post-el.install.
  * Updated Standards-Version to 3.5.6.1.
  * Tweaked smiley regexp not to highlight the leading space.
  
 -- Eric Dorland <eric@debian.org>  Wed, 22 May 2002 01:04:15 -0400

post-el (2002.04.22-4) unstable; urgency=low

  * Tweak emacsen-remove to replace "emacs-goodies-el" to ${PACKAGE}.
  * Unreleased version.

 -- Eric Dorland <eric@debian.org>  Sun, 19 May 2002 15:37:18 -0400

post-el (2002.04.22-3) unstable; urgency=low

  * Apply patch from David Roundy to make the start of a signature be the
    start of a paragraph as well. (Closes: #145243)

 -- Eric Dorland <eric@debian.org>  Sun, 12 May 2002 20:41:28 -0400

post-el (2002.04.22-2) unstable; urgency=high

  * Suggest emacs-goodies-el for mutt-alias.el.
  * urgency high, to get this puppy into woody.

 -- Eric Dorland <eric@debian.org>  Fri, 26 Apr 2002 00:41:27 -0400

post-el (2002.04.22-1) unstable; urgency=low

  * New upstream release.
  * This release just resolves some problems with the last two releases.

 -- Eric Dorland <eric@debian.org>  Tue, 23 Apr 2002 00:57:24 -0400

post-el (2002.04.21-1) unstable; urgency=low

  * New upstream release.
  * Made (require 'cl) unconditional.

 -- Eric Dorland <eric@debian.org>  Mon, 22 Apr 2002 02:31:34 -0400

post-el (2002.04.20-1) unstable; urgency=low

  * New upstream release.
    + smiley highlighting now supported
    + lots of little tweaks to regexps
    + post-url-text-pattern changed to post-url-pattern
  * Fixed non-compiling flet binding.
  * Changed Suggests to Enhances, since this makes more logical sense.

 -- Eric Dorland <eric@debian.org>  Sat, 20 Apr 2002 19:54:17 -0400

post-el (2002.02.08-5) unstable; urgency=low

  * Detection for other mutt tmp files. (Closes: #141078)

 -- Eric Dorland <eric@debian.org>  Wed,  3 Apr 2002 17:42:09 -0500

post-el (2002.02.08-4) unstable; urgency=low

  * Fixed the mutt tmp-file detection regexp. (Closes: #139535)
  * Removed "Cheers," from signature detection regexp.
  * Added README.Debian to explain configuration of post-mode.

 -- Eric Dorland <eric@debian.org>  Sat, 30 Mar 2002 14:43:45 -0500

post-el (2002.02.08-3) unstable; urgency=low

  * Updated post-el.emacsen-startup to now autoload post-mode, not load
    post.el.

 -- Eric Dorland <eric@debian.org>  Thu, 21 Mar 2002 00:03:15 -0500

post-el (2002.02.08-2) unstable; urgency=low

  * Fixed description. (Closes: #138477)
  * Added suggests for slrn.

 -- Eric Dorland <eric@debian.org>  Sun, 17 Mar 2002 13:17:56 -0500

post-el (2002.02.08-1) unstable; urgency=low

  * Initial release. (Closes: #128393)

 -- Eric Dorland <eric@debian.org>  Sat, 23 Feb 2002 14:37:30 -0500

